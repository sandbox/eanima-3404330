<?php

/**
 * Implements hook_token_info().
 */
function computed_ratings_token_info() {
  $info = [];
  $info['types']['computed_ratings'] = [
    'name' => t('Computed Ratings value tokens'),
    'description' => t('Tokens of computed ratings module'),
  ];
  $info['tokens']['computed_ratings']['rating_value'] = [
    'name' => t("Aggregatedd Rating average value"),
    'description' => t('Seo optimised short title.'),
  ];


  return $info;
}


/**
 * Implements hook_tokens().
 */
function computed_ratings_tokens($type, $tokens, array $data, array $options, $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'computed_ratings' && !empty($data['node'])) {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'rating_value':
          $node = $data['node'];
          if ($node->hasField('gesamtwertung_computed') && !$node->get('gesamtwertung_computed')->isEmpty()) {
            $text = $node->gesamtwertung_computed->value;
            $replacements[$original] = $text;
          }
          $replacements[$original] = $text ?? 0;
          break;
      }
    }
  }
  return $replacements;
}
