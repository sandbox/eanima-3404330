<?php

/**
 * @file
 * Contains \Drupal\Core\Field\Plugin\Field\FieldFormatter\PD5CustomFormatter.
 */

namespace Drupal\computed_ratings\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'PD5_Custom' formatter for computed fields Gesamtbewertung.
 *
 * @FieldFormatter(
 *   id = "ComputedRatingsFieldFormatter_ID",
 *   label = @Translation("Rating Formatter computed rating"),
 *   field_types = {
 *     "float",
 *   }
 * )
 */
class ComputedRatingsFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays the star formatting.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   * This generates a "rate" like markup using an iconmoo svg library
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $rounded = round($item->value, 0);

      switch ($rounded) {
        case 0:
          $markup = "<div class='starrating'>";
          $markup .= "<span class='rate-value'>$rounded</span>";
          $markup .= "<span class='rate-image star-off odd s1'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s2'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off odd s3'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s4'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off odd s5'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s6'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "</div>";
          break;

        case 1:
          $markup = "<div class='starrating'>";
          $markup .= "<span class='rate-value'>$rounded</span>";
          $markup .= "<span class='rate-image star-on odd s1'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s2'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off odd s3'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s4'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off odd s5'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s6'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "</div>";
          break;
        case 2:
          $markup = "<div class='starrating'>";
          $markup .= "<span class='rate-value'>$rounded</span>";
          $markup .= "<span class='rate-image star-on odd s1'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on even s2'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off odd s3'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s4'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off odd s5'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s6'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "</div>";
          break;
        case 3:
          $markup = "<div class='starrating'>";
          $markup .= "<span class='rate-value'>$rounded</span>";
          $markup .= "<span class='rate-image star-on odd s1'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on even s2'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on odd s3'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s4'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off odd s5'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s6'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "</div>";
          break;
        case 4:
          $markup = "<div class='starrating'>";
          $markup .= "<span class='rate-value'>$rounded</span>";
          $markup .= "<span class='rate-image star-on odd s1'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on even s2'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on odd s3'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on even s4'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off odd s5'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s6'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "</div>";
          break;
        case 5:
          $markup = "<div class='starrating'>";
          $markup .= "<span class='rate-value'>$rounded</span>";
          $markup .= "<span class='rate-image star-on odd s1'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on even s2'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on odd s3'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on even s4'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on odd s5'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-off even s6'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "</div>";
          break;
        case 6:
          $markup = "<div class='starrating'>";
          $markup .= "<span class='rate-value'>$rounded</span>";
          $markup .= "<span class='rate-image star-on odd s1'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on even s2'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on odd s3'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on even s4'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on odd s5'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "<span class='rate-image star-on even s6'><svg class='icon icon-star-solid'><use href='/sites/all/libs/icomoon/symbol-defs.svg#icon-star-solid'></use></svg></span>";
          $markup .= "</div>";
          break;
      }

      $element[$delta] = ['#markup' => "<b>" . $markup . "</b>"];
    }

    return $element;
  }

}
