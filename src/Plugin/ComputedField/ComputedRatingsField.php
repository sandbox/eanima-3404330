<?php

namespace Drupal\computed_ratings\Plugin\ComputedField;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\computed_field\Field\ComputedFieldDefinitionWithValuePluginInterface;
use Drupal\computed_field\Plugin\ComputedField\ComputedFieldBase;
use Drupal\computed_field\Plugin\ComputedField\SingleValueTrait;

/**
 *
 *
 * @ComputedField(
 *   id = "computed_ratings_field",
 *   label = @Translation("Computed Ratings Field"),
 *   field_type = "float",
 * )
 */
class ComputedRatingsField extends ComputedFieldBase {

  use SingleValueTrait;

  /**
   * {@inheritdoc}
   */
  public function singleComputeValue(EntityInterface $host_entity, ComputedFieldDefinitionWithValuePluginInterface $computed_field_definition): float {
     //return (float) 5.3;

      //This is a small trick to get all fields with a rating value at once
      //If Rate is in the label we get the field and process it
      $rate_fields = array();
      $rating_values = 0;

      //get all fields with rate in it, thats our rating fields
      foreach ($host_entity->getFields() as $fieldname => $fdata) {

        $label = $host_entity->{$fieldname}->getFieldDefinition()->getLabel();

        if (strpos($label, 'Rate') !== FALSE) {
          $rate_fields[$fieldname]['machinename'] = $fieldname;

          //value. only works for solo dropdown list 1 value!
          $value_ = $host_entity->{$fieldname}->getValue();

          //to make sure it exists
          if(isset($value_[0]['value'])) {
            $value = $value_[0]['value'];
          } else {
            $value = 0;
          }

          $rate_fields[$fieldname]['value'] = $value;
          $rating_values = $rating_values + $value;
        }
      }

      //add up fields and divide by amount
      $divideby = count($rate_fields);
      $result = $rating_values / $divideby;

      return $result;
  }

}
